package kz.company.lesson_8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.setTheme(R.style.AppThemeDark)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            Snackbar
                .make(container, R.string.app_snack_hello, 2000)
                .setAction(R.string.app_snack_undo){
                    BottomSheetDialog().show(supportFragmentManager, null)
                }
                .show()
        }
    }
}
